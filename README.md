![Screenshot](https://i.imgur.com/AepHajN.png)
**press F10 to take screenshot

### Download

```sh
git clone git@gitlab.com:joaopedroaats/pers-rEFInd.git refind-theme
```

Copy to /boot

```sh
sudo cp -r refind-theme /boot/efi/EFI/refind/
```


Add `include refind-theme/theme.conf` at the end of `refind.conf`

```sh
sudo nvim /boot/efi/EFI/refind/refind.conf
```
